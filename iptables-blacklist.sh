#!/bin/bash

echo ===================
echo  iptables-backlist
echo ===================
echo
echo The following IP addresses will be blocked:

iptables -D INPUT -j BLACKLIST
iptables -F BLACKLIST
iptables -X BLACKLIST
iptables -N BLACKLIST
iptables -I INPUT -j BLACKLIST


COUNTER=0
for ip in $(grep -v "#" iptables-blacklist.list);
do
    echo - $ip
    iptables -A BLACKLIST -s $ip -j DROP
    let COUNTER++
done
echo "$COUNTER IP addresses have been blocked"


iptables -A BLACKLIST -j RETURN


exit 0
